/*
 * AndroidUtils.h
 *
 *  Created on: Nov 28, 2015
 *      Author: anil
 */

#ifndef ANDROIDUTILS_H_
#define ANDROIDUTILS_H_

class AndroidUtils {
public:
	AndroidUtils();
	virtual ~AndroidUtils();
	void vibrateIt(int duration);
	bool isInternetAvailable();
	void openWebUrl(string url);
	void sendFeedback(string youemail_id, string subject);
	void openPlayAccount(string publiser);
	void rateUs(string app_pakage_name);
};

#endif /* ANDROIDUTILS_H_ */
