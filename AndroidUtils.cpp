/*
 * AndroidUtils.cpp
 *
 *  Created on: Nov 28, 2015
 *      Author: anil
 */

#include "AndroidUtils.h"
#include <jni.h>
#include <platform/android/jni/JniHelper.h>

AndroidUtils::AndroidUtils() {
	// TODO Auto-generated constructor stub

}

AndroidUtils::~AndroidUtils() {
	// TODO Auto-generated destructor stub
}

void AndroidUtils::openWebUrl(string url) {
	cocos2d::JniMethodInfo methodInfo;
	if (JniHelper::getStaticMethodInfo(methodInfo,
			"org/cocos2dx/cpp/AppActivity", "openWebUrl",
			"(Ljava/lang/String;)V")) {

		jstring stringArg1 = methodInfo.env->NewStringUTF(url.c_str());

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
				methodInfo.methodID, stringArg1);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
		methodInfo.env->DeleteLocalRef(stringArg1);
	}
}

void AndroidUtils::sendFeedback(string reciever_emailid, string subject) {
	cocos2d::JniMethodInfo methodInfo;
	if (JniHelper::getStaticMethodInfo(methodInfo,
			"org/cocos2dx/cpp/AppActivity", "sendFeedback",
			"(Ljava/lang/String;Ljava/lang/String;)V")) {

		jstring email = methodInfo.env->NewStringUTF(reciever_emailid.c_str());
		jstring sub = methodInfo.env->NewStringUTF(subject.c_str());

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
				methodInfo.methodID, email, sub);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
//		methodInfo.env->DeleteLocalRef(stringArg1);
	}
}

bool AndroidUtils::isInternetAvailable() {
	cocos2d::JniMethodInfo methodInfo;
	jboolean isAvail = false;
	if (JniHelper::getStaticMethodInfo(methodInfo,
			"org/cocos2dx/cpp/AppActivity", "isInternet", "()Z")) {

		isAvail = methodInfo.env->CallStaticBooleanMethod(methodInfo.classID,
				methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	return isAvail;
}

void AndroidUtils::openPlayAccount(string pubNmae) {
	cocos2d::JniMethodInfo methodInfo;
	if (JniHelper::getStaticMethodInfo(methodInfo,
			"org/cocos2dx/cpp/AppActivity", "openPlayStore",
			"(Ljava/lang/String;)V")) {
		jstring pub = methodInfo.env->NewStringUTF(pubNmae.c_str());
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID,
				pub);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
}

void AndroidUtils::vibrateIt(int time) {
	cocos2d::JniMethodInfo methodInfo;
	if (JniHelper::getStaticMethodInfo(methodInfo,
			"org/cocos2dx/cpp/AppActivity", "vibrateIt", "(I)V")) {
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
				methodInfo.methodID, time);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
}

void AndroidUtils::rateUs(string pakageName) {
	cocos2d::JniMethodInfo methodInfo;
	if (JniHelper::getStaticMethodInfo(methodInfo,
			"org/cocos2dx/cpp/AppActivity", "rateUs",
			"(Ljava/lang/String;)V")) {
		jstring pub = methodInfo.env->NewStringUTF(pakageName.c_str());
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID,
				pub);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
}
